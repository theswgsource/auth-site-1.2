﻿var app = angular.module('app', ['ngRoute', 'ngCookies']);

app.config(function ($routeProvider, $httpProvider) {
    $routeProvider
        .when('/Home',
            {
                templateUrl: 'Views/Common/Home/Home.html',
                controller: 'homeController',
            })
        .when('/RecoverPassword',
            {
                templateUrl: 'Views/Common/RecoverPassword/RecoverPassword.html',
                controller: 'recoverPasswordController',
            })
        .otherwise({ redirectTo: '/Home' });
});


app.factory("utilityService", function ($http) {
    utilityObj = {};

    utilityObj.ServiceUrl = 'http://localhost:3000'
    utilityObj.randomPassword = function () {
        return Math.random().toString(36).substr(2, 5);
    };

    return utilityObj;
})




