﻿app.factory('recoverPasswordService', function ($http, $rootScope) {
    recoverPasswordObj = {};

    recoverPasswordObj.getByPlayer = function (player) {
        var Player;

        Player = $http({
            method: 'GET', url: $rootScope.ServiceUrl + 'RecoverPassword', params: { email: player.Email }
        }).
            then(function (response) {
                return response.data;
            }, function (error) {
                return error.data;
            });

        return Player;
    };

    return recoverPasswordObj;
});

app.controller('recoverPasswordController', function ($scope, recoverPasswordService, utilityService) {
    $scope.RecoverPassword = function (plr, IsValid) {
        console.log(plr);
        if (IsValid) {
            recoverPasswordService.getByPlayer(plr).then(function (result) {
                if (result.ModelState == null) {
                    $scope.Msg = " Your login credentials has been reset. Please check your email.";
                    $scope.Flg = true;
                    $scope.serverErrorMsgs = "";
                    utilityService.myAlert();
                }
                else {
                    $scope.serverErrorMsgs = result.ModelState;
                }
            });
        }
    }
});