﻿app.controller('homeController', function ($scope, $http, utilityService) {
    $scope.user = {};
    $scope.captchaTimer = 0;
    var vm = this;
    vm.register = register;


    function register(user) {
        $http.post(`${utilityService.ServiceUrl}/api/createUser`, { user }).then(function (response) {
            if (response.data.message) {
                vm.successBanner = response.data.message;
            }
            if (response.data.error) {
                vm.errorBanner = response.data.error;
            }
        }, function (error) {
            console.log(`${error}`);
        });
        if (vm.successBanner) {
            setTimeout(function () { vm.successBanner = ''; }, 3000);
        } else if (vm.errorBanner) {
            setTimeout(function () { vm.errorBanner = ''; }, 3000);
        }
    }

    vm.captcha = '';
    makeid();
    var newCaptcha = setInterval(updateTimer, 1000);

    function makeid() {
        $scope.captchaTimer = 0;
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 7; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        vm.captcha = text;
    }

    function updateTimer() {
        $scope.captchaTimer++;
        $scope.$apply();
        if ($scope.captchaTimer === 60) {
            makeid();
        }
    }

});